<?php

//require_once "Mail.php";
/*
 * function to check if user is local 
 */
function checkIfLocal() {
    $whitelist = array(
        '127.0.0.1',
        '::1'
    );

    return in_array(filter_input(INPUT_SERVER, "REMOTE_ADDR"), $whitelist);
}
/*
 * function to get all users
 * param none
 * reurn array of users
 */
function getUsers() {

    global $db;
    $query = 'select * from users';
    $statement = $db->prepare($query);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $users = $statement->fetchAll();
    $statement->closeCursor();

    return $users;
}
/*
 * function to get user by id
 * param userid
 * return user
 */
function getUserByID($userID) {
    global $db;
    $query = 'select * from users where id = :userID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":userID", $userID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $user = $statement->fetch();
    $statement->closeCursor();

    return $user;
}

/*
 * function to log in user
 * @param email and password
 */

function login($email, $password) {

    global $db;
    $query = 'select * from users where email = :email';
    $statement = $db->prepare($query);
    $statement->bindvalue(":email", $email);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $user = $statement->fetch();
    $statement->closeCursor();

    if (password_verify($password, $user["password"])) {
        $_SESSION['userid'] = $user['id'];
        $_SESSION["username"] = $user["username"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["image"] = $user["image"];
        $_SESSION["type"] = $user["type"];
        if (!checkIfLocal()) {
            $from = '<oshiandshaneoop@gmail.com>';
            $to = $_SESSION["email"];
            $subject = 'Log in';
            $body = '<b>Ca Web Programmin</b></br>There was a login to your account from ' . filter_input(INPUT_SERVER, "REMOTE_ADDR") . '</br>If this wasn\'t you , please change your password...';

            sendMail($from, $to, $subject, $body);
        }
        switch ((int) $user["type"]) {
            case 0:
                header("Location:../controller/index.php");
                break;

            case 1:
                header("Location:../admin/index.php?action=show_admin_page");
                break;

            default:
                header("Location:../controller/index.php");
//                echo 'defualt';
                break;
        }
    } else {
        $error = "UserName or Password is Incorrect</br><a href='../admin/'>Login Here</a>";
        include '../view/error.php';
    }
}
/*
 * function to check if user already exists
 * param email
 * return count of users array
 */
function checkUser($email) {
    global $db;
    $query = "select * from users where email = :email";
    $statement = $db->prepare($query);
    $statement->bindValue(":email", $email);
    try {
        $statement->execute();
    } catch (Exception $e) {
        $error = $e->getMessage();
        include '../view/error.php';
    }
    $user = $statement->fetch();

    return count($user);
}
/*
 * function to add user
 * param name email pass type image
 * return none
 */
function addUser($name, $email, $pass, $type, $image) {
    global $db;
    if (checkUser($email) > 0) {
        $error = "User already exists!";
        include '../view/error.php';
        die();
    } else {
        $userId = "null";
        $query = "insert into users values('$userId','$name','$email','$pass','$type','$image')";
        $statement = $db->prepare($query);
        try {
            $statement->execute();
            $_SESSION['username'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION['type'] = $type;
            $_SESSION['image'] = stripslashes($image);
            $_SESSION['userid'] = $db->lastInsertId();
            header("Location:../controller");
        } catch (Exception $ex) {
            //redirect to error.php passing the error message
            $error = $ex->getMessage();
            include ('../view/error.php');
        }
        $statement->closeCursor();
        if (!checkIfLocal()) {
            $from = '<oshiandshaneoop@gmail.com>';
            $to = $email;
            $subject = 'Account Created!';
            $body = '<b>CA web Programmin</b></br>An account has been created for you.</br>Please use your email and password to sign in.';

            sendMail($from, $to, $subject, $body);
        }
    }
}
/*
 * function to update user
 * param name email pass id image
 * return none
 */
function updateUser($name, $email, $pass, $id, $image) {
    global $db;


    $query = "update users set username=:name, email = :email,image = :image where userID = :id";
    $statement = $db->prepare($query);
    $statement->bindvalue(":name", $name);
    $statement->bindvalue(":email", $email);
    $statement->bindvalue(":id", $id);
    $statement->bindvalue(":image", $image);

    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
        exit();
    }
    if ($pass != "") {
        $query = "update user set password = :pass where id = :id";
        $statement = $db->prepare($query);
        $statement->bindvalue(":pass", $pass);
        $statement->bindvalue(":id", $id);
        try {
            $statement->execute();
        } catch (Exception $ex) {
            //redirect to error.php passing the error message
            $error = $ex->getMessage();
            include ('../view/error.php');
            exit();
        }
    }
    $_SESSION['username'] = $name;
    $_SESSION['email'] = $email;
    $statement->closeCursor();
}
/*
 * function to delete user
 * param userid
 * return none
 */
function deleteUser($userId) {
    global $db;
    $query = 'delete  from users where id = :userID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":userID", $userId);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
}
/*
 * function to make admin
 * param id
 * return none
 */
function makeAdmin($userId) {
    global $db;
    $query = 'update users set type = 1 where id = :userID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":userID", $userId);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
}
