<?php
/*
 * Database connection
 */
$dsn = "mysql:host=localhost;dbname=wp_oshi_kavadia";
$username = "root";
$password = "";

try {
    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    error_reporting(E_ALL);
} catch (PDOException $e) {
    $error = $e->getMessage();
    include ('../view/error.php');
    exit();
}
