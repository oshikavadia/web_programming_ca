<?php
/*
 * function to get event comment by id
 * param id
 * return array of comments
 */
function getEComments($eventID) {
    global $db;
    $query = 'select * from event_comments where id = :eventid';
    $statement = $db->prepare($query);
    $statement->bindvalue(":eventid", $eventID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $comments = $statement->fetchAll();
    $statement->closeCursor();

    return $comments;
}
/*
 * add event comment
 * param username comment eventid
 * return none
 */
function addEComment($username, $comment, $eventID) {
    global $db;
    $query = 'insert into event_comments (user_name,event_id,date,text) values (:username,:eventId,now(),:text)';
    $statement = $db->prepare($query);
    $statement->bindvalue(":username", $username);
    $statement->bindvalue(":eventId", $eventID);
    $statement->bindvalue(":text", $comment);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
//    header("Location:../controller/index.php?action=showEvent&eventId=$eventID");
}
/*
 * function to delete comment by id
 * param id
 * return none
 */
function deleteEventCommentByEventID($eventId) {
    global $db;
    $query = "delete from event_comments where event_id = :id";
    $statement = $db->prepare($query);
    $statement->bindValue(":id", $eventId);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
}
