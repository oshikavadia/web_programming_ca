<?php
/*
 * function to get comments by city id
 * param city id
 * return none
 */
function getComments($cityID) {
    global $db;
    $query = 'select * from cities_comments where city_id = :cityID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":cityID", $cityID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $comments = $statement->fetchAll();
    $statement->closeCursor();

    return $comments;
}

/*
 * function to add comment 
 * param username comment cityid
 * return none
 */
function addComment($username, $comment, $cityID) {
    global $db;
    $query = 'insert into cities_comments (city_id,user_name,date,text) values (:cityId,:username,now(),:text)';
    $statement = $db->prepare($query);
    $statement->bindvalue(":username", $username);
    $statement->bindvalue(":cityId", $cityID);
    $statement->bindvalue(":text", $comment);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
//    header("Location:../controller/index.php?action=showCity&cityID=$cityID");
}

/*
 * function to delete comment by city id
 * param city id
 * return none
 */
function deleteCityCommentByCityId($cityID) {
    global $db;
    $query = "delete from cities_comments where cities_comments.city_id = :cityID";
    $statement = $db->prepare($query);
    $statement->bindValue(":cityID", $cityID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
}
