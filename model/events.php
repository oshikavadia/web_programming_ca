<?php
/*
 * function to get all events
 * param none
 * return array of events
 */
function getEvents() {
    global $db;
    $query = 'select * from events group by city_id';
    $statement = $db->prepare($query);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $events = $statement->fetchAll();
    $statement->closeCursor();

    return $events;
}
/*
 * function to get events by id
 * param eventid
 * return events
 */
function getEventByID($eventID) {
    global $db;
    $query = 'select * from events where id = :eventID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":eventID", $eventID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $event = $statement->fetch();
    $statement->closeCursor();

    return $event;
}

/*
 * function to get event by cityId
 * param cityid
 * return array of event
 */
function getEventsByCity($cityID) {
    global $db;
    $query = 'select * from events where city_id = :cityID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":cityID", $cityID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
//        echo $error;
        include ('../view/error.php');
    }
    $events = $statement->fetchAll();
    $statement->closeCursor();

    return $events;
}
/*
 * function to add event
 * param cityid nae desc date image
 * return none
 */
function addEvent($cityId, $name, $desc, $date, $image) {
    global $db;
    $query = "insert into events values('null','$cityId','$name','$desc','$date','$image')";
    $statement = $db->prepare($query);
    try {
        $statement->execute();
//        header("Location:../controller/index.php?action=showCity&cityID=$cityId");
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
    header("Location:../controller/index.php?action=showCity&cityID=$cityId");
}
/*
 * function to delete event by cityid
 * param city id
 * return none
 */
function deleteEventByCityID($cityID) {
    global $db;
    $query = "delete from events where city_id = :cityID";
    $statement = $db->prepare($query);
    $statement->bindValue(":cityID", $cityID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
}

/*
 * function to delete event by id
 * param id
 * return none
 */
function deleteEvent($id) {
    global $db;
    $query = "delete from events where id = :id";
    $statement = $db->prepare($query);
    $statement->bindValue(":id", $id);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
}
