<?php
/*
Function to get City
return array of cities
param none
*/
function getCities() {
    global $db;
    $query = 'select * from cities';
    $statement = $db->prepare($query);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $cities = $statement->fetchAll();
    $statement->closeCursor();

    return $cities;
}
/*
 * Function to get city by id
 * param city id
 * return city
 */
function getCityByID($cityID) {
    global $db;
    $query = 'select * from cities where id = :cityID';
    $statement = $db->prepare($query);
    $statement->bindvalue(":cityID", $cityID);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $cities = $statement->fetch();
    $statement->closeCursor();

    return $cities;
}
/*
 * function to add city
 * param name, desc,long,lat,image
 * return none
 */
function addCity($name, $desc, $long, $lat, $image) {
    global $db;
    $query = "insert into cities values('null','$name','$desc','$long','$lat','$image')";
    $statement = $db->prepare($query);
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
    $id = $db->lastInsertId();
    header("Location:../controller/index.php?action=showCity&cityID=$id");
}
/*
 * function to delete city
 * param cityid
 * return none
 */
function deleteCity($cityID) {
    global $db;

    $query = "delete from cities where cities.id = :cityID";
    $statement = $db->prepare($query);
    $statement->bindValue(":cityID", $cityID);


    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
    }
    $statement->closeCursor();
    header("Location:../admin/index.php?action=viewCities");
}
/*
 * function to update city
 * param id , desc, name, long, lat, image, changeImage
 * return none
 */
function updateCity($id, $name, $desc, $long, $lat, $image, $changeImage) {
    global $db;
    if ($changeImage == 0) {
        $query = 'update cities set name = :name,description= :desc,longitude = :long,latitude = :lat  where id = :id';
        $statement = $db->prepare($query);
        $statement->bindvalue(":id", $id);
        $statement->bindvalue(":name", $name);
        $statement->bindvalue(":desc", $desc);
        $statement->bindvalue(":long", $long);
        $statement->bindvalue(":lat", $lat);
    } else {
        $query = "update cities set name = '$name',description='$desc',longitude = '$long',latitude = '$lat', image = '$image' where id = '$id'";
        //echo $query;
        $statement = $db->prepare($query);
    }
    try {
        $statement->execute();
    } catch (Exception $ex) {
        //redirect to error.php passing the error message
        $error = $ex->getMessage();
        include ('../view/error.php');
        exit();
    }
    $statement->closeCursor();
}
