<?php

session_start();
date_default_timezone_set("UTC");
require_once '../model/cities.php';
require_once '../model/cities_comments.php';
require_once '../model/database.php';
require_once '../model/events.php';
require_once '../model/events_comments.php';
require_once '../model/mailerUtility.php';
require_once '../model/users.php';


$action = filter_input(INPUT_POST, "action");

if ($action == null) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == null) {
        $action = 'home';
    }
}

switch ($action) {
    case "home":
        $cities = getCities();
        include '../view/home.php';
        break;

    case "showCity":
        $cityID = filter_input(INPUT_GET, 'cityID', FILTER_VALIDATE_INT);
        $city = getCityByID($cityID);
        $comments = getComments($cityID);
        $events = getEventsByCity($cityID);

        include '../view/city.php';
        break;

    case "login":
        header('Location:../admin/index.php?action=show_login_form');
        break;

    case "addCityComment":
        $cityId = filter_input(INPUT_POST, 'cityid', FILTER_VALIDATE_INT);
        $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
        $text = filter_input(INPUT_POST, 'comment');
        addComment($username, $text, $cityId);
        break;
    case "showEvent" :
        $eventId = filter_input(INPUT_GET, 'eventId', FILTER_VALIDATE_INT);
        $event = getEventByID($eventId);
        $comments = getEComments($eventId);
        include '../view/events.php';
        break;
    case "addEventComment":
        $eventId = filter_input(INPUT_POST, 'eventId', FILTER_VALIDATE_INT);
        $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
        $text = filter_input(INPUT_POST, 'comment');
        addEComment($username, $text, $eventId);
        break;
}