
<footer>
    <p>&copy; <?php
        date_default_timezone_set('UTC');
        echo date('Y') . ' ';
        if (!(filter_input(INPUT_GET, "action") == "show_login_form")) {
            echo '<a href="../controller/index.php?action=login">Login</a>';
        }
        ?></p>
</footer>       
</body>
</html>