<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include 'header.php';
?>

<main>


    <table>
        <tr>
            <th>Users</th>
            <th>User Type</th>
            <th>Update/Delete</th>
        </tr>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?php echo $user['username']; ?></td>
                <td><?php echo $user['type']; ?></td>

                <td>
                    <form action="../admin/index.php" method="post">
                        <input type="hidden" name="action" value="deleteUser"/>
                        <input type="hidden" name="id" value="<?php echo $user['id']; ?>"/>
                        <input type="Submit" value="Delete">
                    </form>
                    <form action="../admin/index.php" method="post">
                        <input type="hidden" name="action" value="makeAdmin"/>
                        <input type="hidden" name="id" value="<?php echo $user['id']; ?>"/>
                        <input type="Submit" value="Make Admin"/>
                    </form>
                </td>



            </tr>
        <?php endforeach; ?>

    </table>

</main>
<?php
include 'footer.php';
?>
