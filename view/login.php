<?php
include 'header.php';
?>

<main>
    <h1>
        Login
    </h1>
    <?php
    if (!isset($_SESSION['username'])) {
        echo'<form action="index.php" method="post">
        <input type="hidden" name="action" value="login">
        <label for="email">User Email : </label>
        <input type="text" name="email" required=""></br>
        <label for="password">Password : </label>
        <input type="password" name="password" required=""></br>
        <input type="submit" value="Login"> 
    </form></br>
    <button id="register" onclick="location.href =\'../admin/index.php?action=show_register_form\'">Register</button>
      ';
    } else {
        echo '<p>You are already logged in ! If you want to log in as another user please logout . </br>'
        . '<a href="../admin/index.php?action=logout">Log Out</a>'
        . '</p>';
    }
    ?>
</main>
<?php
include 'footer.php';

