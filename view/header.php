<?php
//session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="../css/main.css" rel="stylesheet" type="text/css"/> 
        <script src="../js/jquery-2.2.3.js" type="text/javascript"></script>
        <script src="../js/common.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <a href="../controller/index.php"><img src="../images/logo.png" alt="logo" id="logo"></a>
        </header>
    <user><?php
//    '<img src="data:image/png;base64,' . base64_encode($car['image']) . '" height = 100px width = 200px/>'
        if (isset($_SESSION['username'])) {
            echo '<div class="dropdown" >
  <button class="dropbtn img-circle" id="userPic">' . $_SESSION['username'] . '</button>
  <div class="dropdown-content">
    <a href="../admin/index.php?action=logout">Logout</a>
     </div>
</div>';
            echo '<script>$(document).ready(function () {'
            . '$("#userPic").css("background-image", "url(data:image/png;base64,' . base64_encode($_SESSION['image']) . '"); });</script>';
        }
        ?>
    </user>
    <?php
    include 'menu.php';
    