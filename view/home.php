<?php
include 'header.php';
?>
<main>

</main>
<table>
    <tr>
        <?php
        foreach ($cities as $city):
            ?>
            <td><?php
                echo $city['name'];
                echo '<a href=".?action=showCity&cityID=' . $city['id'] . '">';
                echo "<img class=\"home_thumb\" src=\"data:image/png;base64," . base64_encode($city['image']) . "\" alt=\"" . $city['name'] . "\">";
                echo '</a>';
                ?></td>
        <?php endforeach; ?>
    </tr>
</table>
<?php
include 'footer.php';
