<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include ('header.php');
?>

<main>
    <h1>
        Update City
    </h1>

    <form action="index.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="update_city">

        <input type="hidden" name="id" value="<?php echo $cityID ?>"></br>
        <label for="name">New name: </label>
        <input type="text" name="name" value="<?php echo $city['name']; ?>"></br>
        <label for="description">New description: </label>
        <input type="text" name="description" value="<?php echo $city['description']; ?>"></br>
        <label for="latitude">New latitude: </label>
        <input type="number" name="latitude" value="<?php echo $city['latitude']; ?>"></br>
        <label for="longitude">New Cost: </label>
        <input type="number" name="longitude" value="<?php echo $city['longitude']; ?>"></br>
        <label for="image">Leave empty if same image: </label>
        <input type="file" name="image"></br>
        <input type="submit" value="Update City">
    </form>
</main>
<?php
include ('footer.php');
