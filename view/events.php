<?php
include '../view/header.php';
?>
<script>

    $(document).ready(function () {
        $("#submitComment").click(function () {
            console.log($('#id').val());
            $.ajax({
                url: "../controller/index.php",
                type: "POST",
                data: {
                    'action': "addEventComment",
                    'eventId': $('#id').val(),
                    'comment': $("#commentBox").val(),
                    'username': $("#username").val(),
                    'date': $("#date").val()
//                    'cityId': $("#cityid").val()
                },
                success: function (data) {
                    $("#noComments").hide();
                    var commentString = "</br>" + $("#username").val() + " on " + $("#date").val() + " </br>" + $("#commentBox").val();
                    $("#comments").append(commentString);
                    console.log(commentString);
                }, error: function () {
                    $("#comments").html("Ajax error");
                }

            });
        });
    });


</script>
<main>


    <h1><?php echo $event['name']; ?></h1>
    </br>
    <p>
        <?php echo $event['description']; ?></br>
        Date :<?php echo $event['date']; ?>
        <img id="eventImage" src="data:image/png;base64,<?php echo base64_encode($event['image']); ?>" alt="<?php echo $event['name']; ?>" width="300px" height="350px"/>
    </p>
    <comments>
        <h3>Comments</h3>
        <?php
        echo '<div id="comments">';
        if ($comments == null) {
            echo "<p>No Comments found</p>";
        } else {

            foreach ($comments as $comment) {

                echo '</br>';
                echo $comment['user_name'] . ' on ' . $comment['date'] . '</br>' . $comment['text'];
            }
        }echo '</div>';
        if (isset($_SESSION['username'])) {
            $id = $event['id'];
            include '../view/add_comment.php';
        }
        ?>
        <!--<input type="hidden" id="cityid" value="<?php // echo $event['cityid'];    ?>">-->

    </comments>
</main>
<?php
include '../view/footer.php';
?>