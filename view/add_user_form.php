<?php

include 'header.php';
?>
<script>
    $(document).ready(function () {
        $("#passwordCheck").keyup(function () {
            var password1 = $("#password").val();
//            console.log(password1);
            var password2 = $("#passwordCheck").val();
            console.log(password1 == password2);
            if (!password1 == password2) {
                $("#registerSubmit").css("visibility", "hidden");

            }
            if (password1 == password2) {
                $("#registerSubmit").css("visibility", "visible");
            }

        });
    });



</script>

<main>
    <h1>
        Adding a new User
    </h1>

    <section>

    </section>
    <form action="../admin/index.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="add_user">
        <label for="name"> Name : </label>
        <input type="name" name="name" required="" pattern="[A-Z][a-z]+( [A-Z][a-z]+)?"></br>
        <label for="email">User Email : </label>
        <input type="text" name="email" required="" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"></br>
        <label for="password">Password : </label>
        <input type="password" name="password" id="password" required=""></br>
        <label for="passwordCheck">Verify Password: </label>
        <input type="password" name="passwordCheck" id="passwordCheck"required="" ></br>
        <label>Profile Picture:</label>
        <input type="file" name="image" required accept="image/*"></br>
        <input type="submit" value="Register Now" id="registerSubmit">
    </form>

</main>
<?php

include 'footer.php';
?>
