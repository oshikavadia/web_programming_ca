<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include '../view/header.php';
?>
<main>
    <h1>
        Add a City
    </h1>
    <form id="addCity" enctype="multipart/form-data" method="post" action="../admin/index.php">
        <input type="hidden" name="action" value="add_city" >
        <label for="name">City Name :</label>
        <input type="text" name="name" required placeholder="City Name"></br>
        <label for="desc">City Description :</label>
        <input type="text" name="desc" required placeholder="City Description"></br>
        <label for="latitude">Latitude : </label>
        <input type="number" name="latitude" required step="any"></br>
        <label for="longitude">Longitude : </label>
        <input type="number" name="longitude" required step="any"></br>
        <label for="image">Image :</label>
        <input type="file" required name="image"></br>
        <input type="submit" value="Add City">
    </form>
</main>
<?php
include '../view/footer.php';
