<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include 'header.php';
?>
<main>
    <button name="addCity" onclick="location.href = '../admin/index.php?action=show_add_city_form'">Add City</button>
    <button name="addEvent" onclick="location.href = '../admin/index.php?action=show_add_event_form'">Add Event</button>
    <button name="viewUsers" onclick="location.href = '../admin/index.php?action=viewUsers'">View Users</button>
    <button name="viewCities" onclick="location.href = '../admin/index.php?action=viewCities'">View Cities</button>
    <button name="viewEvents" onclick="location.href = '../admin/index.php?action=viewEvents'">View Events</button>
</main>
<?php
include 'footer.php';
