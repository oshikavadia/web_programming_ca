<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include '../view/header.php';
?>
<main>
    <h1>
        Add an Event
    </h1>
    <form id="addEvent" enctype="multipart/form-data" method="post">
        <select name="cityId" id="cityId">
            <?php
            foreach (getCities() as $city) {
                echo '<option value=' . $city['id'] . '>' . $city['name'] . '</option>';
            }
            ?>
        </select></br>
        <input type="hidden" name="action" value="add_event" >
        <!--<input type="hidden" name="cityId" value="<?php // $cityID     ?>">-->
        <label for="name">Event Name :</label>
        <input type="text" name="name" required placeholder="Event Name"></br>
        <label for="name">Event Description :</label>
        <input type="text" name="desc" required placeholder="Event Description"></br>
        <label for="date">Event Date : </label>
        <input type="date" name="date" required ></br>
        <label for="image">Image :</label>
        <input type="file"  name="image" required></br>
        <input type="submit" value="Add Event">
    </form>
</main>
<?php
include '../view/footer.php';
