<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include 'header.php';
?>

<main>


    <table>
        <tr>
            <th>Event Name</th>
            <th>Delete</th>
        </tr>
        <?php foreach ($events as $event) : ?>
            <tr>
                <td><?php echo $event['name']; ?></td>

                <td>
                    <form action="../admin/index.php" method="post">
                        <input type="hidden" name="action" value="delete_event"/>
                        <input type="hidden" name="id" value="<?php echo $event['id']; ?>"/>
                        <input type="Submit" value="Delete">
                    </form>
                </td>



            </tr>
        <?php endforeach; ?>

    </table>

</main>
<?php
include 'footer.php';
?>
