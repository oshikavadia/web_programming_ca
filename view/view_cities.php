<?php
if (!$_SESSION['type'] == 1) {
    $error = "You dont have enough rights to view this page";
    include '../view/error.php';
    die();
}
include 'header.php';
?>

<main>


    <table>
        <tr>
            <th>City</th>
            <th>Update/Delete</th>
        </tr>
        <?php foreach ($cities as $city) : ?>
            <tr>
                <td><?php echo $city['name']; ?></td>

                <td>
                    <form action="../admin/index.php" method="post">
                        <input type="hidden" name="action" value="delete_city"/>
                        <input type="hidden" name="id" value="<?php echo $city['id']; ?>"/>
                        <input type="Submit" value="Delete">
                    </form>
                    <form action="../admin/index.php" method="post">
                        <input type="hidden" name="action" value="show_update_city"/>
                        <input type="hidden" name="id" value="<?php echo $city['id']; ?>"/>
                        <input type="Submit" value="Update"/>
                    </form>
                </td>



            </tr>
        <?php endforeach; ?>

    </table>

</main>
<?php
include 'footer.php';
?>
