<?php
include 'header.php';
?>
<script>
//    var date = new Date(year, month, day);
    $('body').css("background-image", "url(data:image/png;base64,<?php echo base64_encode($city['image']); ?>)");
    $(document).ready(function () {
        $("#submitComment").click(function () {
            $.ajax({
                url: "../controller/index.php",
                type: "POST",
                data: {
                    'action': "addCityComment",
                    'cityid': $('#id').val(),
                    'comment': $("#commentBox").val(),
                    'username': $("#username").val(),
                    'date': $("#date").val()
                },
                success: function (data) {
                    $("#noComments").hide();
                    var commentString = "</br>" + $("#username").val() + " on " + $("#date").val() + " </br>" + $("#commentBox").val();
                    $("#comments").append(commentString);
                    console.log(commentString);
                }, error: function () {
                    $("#comments").html("Ajax error");
                }

            });
        });
    });


</script>
<main>
    <h1 id="cityTitle">
        <?php echo $city['name']; ?>
    </h1>
    <main>
        <p>
            <?php echo $city['description']; ?>
        </p>

    </main>
    <map>
        <iframe width="600" height="340" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $city['latitude'] ?>,<?php echo $city['longitude'] ?>&hl=es;z=3&amp;output=embed"></iframe><br />
    </map>
    <comments>
        <h3>Comments</h3>
        <?php
        echo '<div id="comments">';
        if ($comments == null) {
            echo '<p id="noComments">No Comments found<p>';
        } else {

            foreach ($comments as $comment) {
                echo '</br>';
                echo $comment['user_name'] . ' on ' . $comment['date'] . '</br>' . $comment['text'];
            }
        } echo '</div>';
        if (isset($_SESSION['username'])) {
            $id = $cityID;
            include '../view/add_comment.php';
        }
        ?>


    </comments>
    <events>
        <h3>Events</h3>
        <?php
        if ($events == null) {
            echo "<p>No Events found</p>";
        } else {
            echo '<div id="event"';
            echo '</br><ul>';
            foreach ($events as $event) {

                echo '<li><a href="../controller/index.php?action=showEvent&eventId=' . $event['id'] . '">' . $event['name'] . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
    </events><?php
//    if (isset($_SESSION['username'])) {
//        echo '<button onclick="location.href =\'../admin/index.php?action=add_event_form&cityID='.$city['id'].'\'"';
//    }


    include 'footer.php';
    