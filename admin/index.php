<?php

session_start();
require_once '../model/users.php';
require_once '../model/database.php';
require_once '../model/cities.php';
require_once '../model/events.php';
require_once '../model/events_comments.php';
require_once '../model/cities_comments.php';

$action = filter_input(INPUT_POST, "action");

if ($action == null) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == null) {
        $action = 'show_login_form';
    }
}

switch ($action) {
    case "show_login_form":
        include '../view/login.php';
        break;
    case "show_register_form":
        include '../view/add_user_form.php';
        break;
    case "add_user":

        $type = filter_input(INPUT_POST, "type");
        if ($type == null) {
            $type = 0;
        }

        $username = filter_input(INPUT_POST, 'name');
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $password = filter_input(INPUT_POST, 'password');
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
        if ($username == null || $email == null || $password == null || $image == null) {
            $error = "Please check your inputs then try again.";
            include '../view/error.php';
        } else {
            addUser($username, $email, $passwordHash, $type, $image);
        }
        break;

    case "login":
        $email = filter_input(INPUT_POST, 'email');
        $password = filter_input(INPUT_POST, 'password');
        if ($email == null || $password == null) {
            $error = "Please check your credentials and try again...</br><a href='../admin/'>Login Here</a>";
            include '../view/error.php';
        } else {
            login($email, $password);
        }
        break;
    case "show_admin_page":
        include '../view/admin.php';
        break;
    case "logout":
        session_destroy();
        header("Location:../admin");
        break;
    case "show_add_event_form":
        include '../view/add_event_form.php';
        break;
    case "add_event":
        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
        $desc = filter_input(INPUT_POST, "desc", FILTER_SANITIZE_STRING);
        $date = filter_input(INPUT_POST, "date");
        $cityID = filter_input(INPUT_POST, "cityId", FILTER_VALIDATE_INT);
        $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
        if ($name == null || $desc == null || $date == null || $cityID == null || $image == null) {
            $error = "Please check your inputs and try again";
            include '../view/error.php';
        } else {
            addEvent($cityID, $name, $desc, $date, $image);
        }
        break;
    case "show_add_city_form" :
        include '../view/add_city_form.php';
        break;
    case "add_city":
        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
        $desc = filter_input(INPUT_POST, "desc", FILTER_SANITIZE_STRING);
        $latitude = filter_input(INPUT_POST, "latitude", FILTER_VALIDATE_FLOAT);
        $longitude = filter_input(INPUT_POST, "longitude", FILTER_VALIDATE_FLOAT);
        $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
        if ($name == null || $desc == null || $latitude == null || $longitude == null || $image == null) {
            $error = "Please check your inputs and try again";
            include '../view/error.php';
        } else {
            addCity($name, $desc, $longitude, $latitude, $image);
        }
        break;
    case "viewCities":
        $cities = getCities();
        include '../view/view_cities.php';
        break;
    case "delete_city":
        $cityID = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        deleteCityCommentByCityId($cityID);
        deleteEventByCityID($cityID);
        deleteEventCommentByCityId($cityID);
        deleteCity($cityID);
        break;
    case "show_update_city":
        $cityID = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        $city = getCityByID($cityID);
        include '../view/city_update_form.php';
        break;
    case "update_city":
        $updateImage = 0;
        $image = null;
        $id = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
        $desc = filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
        $latitude = filter_input(INPUT_POST, "latitude", FILTER_VALIDATE_FLOAT);
        $longitude = filter_input(INPUT_POST, "longitude", FILTER_VALIDATE_FLOAT);
        if ($_FILES['image']['tmp_name'] != null) {
            $updateImage = 1;
            $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
        }
        if ($name == null || $desc == null || $latitude == null || $longitude == null) {
            $error = "Please check your inputs and try again";
            include '../view/error.php';
        } else {
            updateCity($id, $name, $desc, $longitude, $latitude, $image, $updateImage);
            $cities = getCities();
            include '../view/view_cities.php';
        }

        break;
    case "viewUsers":
        $users = getUsers();
        include '../view/view_users.php';
        break;
    case "deleteUser":
        $users = getUsers();
        $userId = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        deleteUser($userId);
        include '../view/view_users.php';
        break;
    case "makeAdmin":
        $users = getUsers();
        $userId = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        makeAdmin($userId);
        include '../view/view_users.php';
        break;
    case "viewEvents":
        $events = getEvents();
        include '../view/view_events.php';
        break;
    case "delete_event":
        $eventId = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
        deleteEventCommentByEventID($eventId);
        deleteEvent($eventId);
        $events = getEvents();
        include '../view/view_events.php';
        break;
}